# -----------------------------------------------------------
# This script is the main generator for all the RF Geometries
# TODO: Separate each geometry into its own class and handle the generation 
# from here
#
# (C) I don't know, insert something here maybe? I want everyone to be able to
# use my code if it's helpful
# -----------------------------------------------------------
from decimal import ROUND_DOWN
import sys
import os
import pathlib
import argparse
from numpy import angle, size
import yaml
import math

sys.path.append(os.path.join(sys.path[0], "..", ".."))  # load parent path of KicadModTree
sys.path.append(os.path.join(sys.path[0], "..", "tools"))  # load parent path of tools

from KicadModTree import *  # NOQA
from KicadModTree.nodes.base.Pad import Pad  # NOQA

class RFGeometry():

    RAT_RACE_KEY = 'RatRace'
    RADIAL_STUB_KEY = 'RadialStub'

    FP_NAME_KEY = 'fp_name'
    FP_LIB_KEY = 'fp_lib_name'
    FP_PREFIX_KEY = 'prefix'
    FP_DESCRIPTION_KEY = 'description'
    FP_KEYWORDS_KEY = 'keywords'
    FP_PARAMS_KEY = 'parameters'
    TEXT_LAYER_KEY = 'layer'
    TEXT_SIZE_KEY = 'size'
    TEXT_FONTWIDTH_KEY = 'fontwidth'
    TEXT_THICKNESS_KEY = 'thickness_factor'
    TEXT_POSITION_KEY = 'position_y'
    TEXT_POSITION_INSIDE = 'inside'
    TEXT_POSITION_OUTSIDE_TOP = 'outside_top'
    TEXT_POSITION_OUTSIDE_BOT = 'outside_bottom'

    PARAMS_UNITS_KEY = 'units'
    PARAMS_NO_SMASK_KEY = 'remove_soldermask'
    PARAMS_SMASK_OFFSET_KEY = 'soldermask_offset'

    # Rat Race Coupler Keys
    PARAMS_RR_RADIUS_KEY = 'radius'
    PARAMS_RR_WIDTH_KEY = 'trace_width'

    # Radial Stub Keys
    PARAMS_RS_WI_KEY = 'Wi'
    PARAMS_RS_L_KEY = 'L'
    PARAMS_RS_ANGLE_KEY = 'angle_deg'

    _VALID_UNITS = ['mm', 'mils']

    TEXT_ROUND_PRECISION = 0.05
    TEXT_OFFSET_MM = 0.25

    def __init__(self, geo_description_filepath, config_filepath, 
                 lib_root_folderpath) -> None:
        # Read in config file
        with open(config_filepath) as config_file:
            configs = yaml.safe_load(config_file)

        self.fab_line_width_mm = configs['fab_line_width']
        self.courtyard_line_width_mm = configs['courtyard_line_width']
        # references and values are arrays of dicts describing the layer, 
        # position, size, and fontwidth for the reference designator 
        self.ref_text_info = configs['references']
        self.value_text_info = configs['values']

        # Read in geometric descriptions
        with open(geo_description_filepath) as desc_file:
            self.geometries = yaml.safe_load(desc_file)

        # Library root path
        self.lib_root = lib_root_folderpath

    def makeFootprints(self):
        for geometry in self.geometries:
            if geometry == RFGeometry.RAT_RACE_KEY:
                for rr_def in self.geometries[geometry]:
                    # Generate kicad_mod footprint
                    kicad_mod = self.generateRatRace(rr_def)

                    # Get/make directory to footprint library
                    lib_dir = pathlib.Path(self.lib_root)
                    lib_dir.mkdir(parents=True, exist_ok=True)

                    filename = kicad_mod.name + '.kicad_mod'
                    fh = KicadFileHandler(kicad_mod)
                    fh.writeFile(str(lib_dir / filename))
            elif geometry == RFGeometry.RADIAL_STUB_KEY:
                for rs_def in self.geometries[geometry]:
                    # Generate kicad_mod footprint
                    kicad_mod = self.generateRadialStub(rs_def)

                    # Get/make directory to footprint library
                    lib_dir = pathlib.Path(self.lib_root)
                    lib_dir.mkdir(parents=True, exist_ok=True)

                    filename = kicad_mod.name + '.kicad_mod'
                    fh = KicadFileHandler(kicad_mod)
                    fh.writeFile(str(lib_dir / filename))
            else:
                raise ValueError("Geometry {:s} is not currently supported".format(geometry))

    def generateRadialStub(self, definition):
        # TODO: Document. Based on ADS definition: https://edadocs.software.keysight.com/pages/viewpage.action?pageId=5693198
        
        # Load in the radial stub geometry parameters
        parameters = definition[RFGeometry.FP_PARAMS_KEY]
        units = parameters[RFGeometry.PARAMS_UNITS_KEY] 
        theta_deg = parameters[RFGeometry.PARAMS_RS_ANGLE_KEY]
        length = parameters[RFGeometry.PARAMS_RS_L_KEY] # L in drawing
        trace_width = parameters[RFGeometry.PARAMS_RS_WI_KEY] # Wi in drawing
        length_mm = self.unitToMm(length, units)
        trace_width_mm = self.unitToMm(trace_width, units)

        # Determine soldermask parameters
        remove_soldermask = parameters.get(RFGeometry.PARAMS_NO_SMASK_KEY, True)
        if remove_soldermask:
            soldermask_offset = parameters.get(RFGeometry.PARAMS_SMASK_OFFSET_KEY, None)
            if soldermask_offset is not None:
                soldermask_offset_mm = self.unitToMm(soldermask_offset, units)
            else:
                soldermask_offset_mm = 3*trace_width_mm
        else:
            soldermask_offset_mm = 0

        # Get library information
        fp_name = definition[RFGeometry.FP_NAME_KEY]
        fp_name = fp_name.format(l=length, wi=trace_width, 
                                    theta=theta_deg, units=units)
        description = definition[RFGeometry.FP_DESCRIPTION_KEY]
        keywords = definition[RFGeometry.FP_KEYWORDS_KEY]

        # Delete the unknown unit length and width variables to make sure they 
        # don't get used
        del length
        del trace_width

        # Initialize footprint
        kicad_mod = Footprint(fp_name)
        kicad_mod.setDescription(description)
        kicad_mod.setTags(keywords)
        kicad_mod.setAttribute('smd')

        # ---- Define copper layer ---- #
        # Nodes need to be shifted down since pad anchor is in center of base pad
        stub_pad_height_mm = trace_width_mm/2
        stub_poly_y_offset_mm = -stub_pad_height_mm/2
        stub_nodes = self.calcRadialStubPolyNodes(length_mm, trace_width_mm, 
                                        theta_deg, max_arc_seg_length_mm=0.2,
                                        origin_mm=(0, stub_poly_y_offset_mm))
        stub_cu_poly = Polygon(nodes=stub_nodes, layer='F.Cu', width=0)
        stub_pad = Pad(number=1, at=[0, -stub_pad_height_mm/2], 
                        type=Pad.TYPE_SMT, layers=['F.Cu'],
                        size=[trace_width_mm, stub_pad_height_mm], 
                        shape=Pad.SHAPE_CUSTOM, anchor_shape=Pad.ANCHOR_RECT,
                        primitives=[stub_cu_poly])
        kicad_mod.append(stub_pad)

         # ---- Define fabrication layer ---- #
        # Add the fabrication layer
        fab_lines = self._calcStubFabLines(trace_width_mm, length_mm, theta_deg)
        for line in fab_lines:
            kicad_mod.append(line)

        # ---- Define soldermask layer ---- #
        if remove_soldermask:
            l_smask = length_mm + 2*soldermask_offset_mm
            w_smask = trace_width_mm + 2*soldermask_offset_mm
            y_offset_smask = -soldermask_offset_mm
            smask_nodes = self.calcRadialStubPolyNodes(l_smask, w_smask, 
                                        theta_deg, max_arc_seg_length_mm=0.2,
                                        origin_mm=(0, y_offset_smask))
            smask = Polygon(nodes=smask_nodes, layer='F.Mask', width=0)
            kicad_mod.append(smask)

        # ---- Add Text ---- #
        bounding_box = smask.calculateBoundingBox() if remove_soldermask \
                                     else stub_cu_poly.calculateBoundingBox()
        self._appendTextRS(kicad_mod, bounding_box, fp_name)
        
        return kicad_mod

    def _appendTextRS(self, kicad_mod, bounding_box, fp_name):
        # References
        # TODO: Update so the F.Fab layer Ref** is dynamically sized
        # TODO: Once this is moved to its own class, all the parameters should
        #       be contained in self

        bb_max_mm = bounding_box['max']
        bb_min_mm = bounding_box['min']
        text_x_mm = 0
        for text_info in self.ref_text_info:
            layer = text_info[RFGeometry.TEXT_LAYER_KEY]
            position_y = text_info[RFGeometry.TEXT_POSITION_KEY]
            size = text_info[RFGeometry.TEXT_SIZE_KEY]
            fontwidth = text_info[RFGeometry.TEXT_FONTWIDTH_KEY]

            # Calculate the text y offset from bounding box
            text_y_offset_mm = size[0]/2 + RFGeometry.TEXT_OFFSET_MM
            if position_y == RFGeometry.TEXT_POSITION_OUTSIDE_TOP:
                text_y_mm = bb_min_mm.y - text_y_offset_mm # Because -y is up
            elif position_y == RFGeometry.TEXT_POSITION_OUTSIDE_BOT:
                text_y_mm = bb_max_mm.y + text_y_offset_mm
            elif position_y == RFGeometry.TEXT_POSITION_INSIDE:
                text_x_mm = (bb_max_mm.x + bb_min_mm.x) / 2
                text_y_mm = (bb_min_mm.y + bb_max_mm.y) / 2

            text_y_mm = RFGeometry.roundToBase(text_y_mm, 
                                        RFGeometry.TEXT_ROUND_PRECISION)
            text_x_mm = RFGeometry.roundToBase(text_x_mm, 
                                        RFGeometry.TEXT_ROUND_PRECISION)

            if layer == 'F.SilkS':
                kicad_mod.append(Text(type=Text.TYPE_REFERENCE, text='REF**', 
                                      at=[text_x_mm, text_y_mm], layer=layer, 
                                      size=size, thickness=fontwidth))
            else:
                kicad_mod.append(Text(type=Text.TYPE_USER, text='${REFERENCE}', 
                                      at=[text_x_mm, text_y_mm], layer=layer, 
                                      size=size, thickness=fontwidth))

        # Values
        for text_info in self.value_text_info:
            layer = text_info[RFGeometry.TEXT_LAYER_KEY]
            position_y = text_info[RFGeometry.TEXT_POSITION_KEY]
            size = text_info[RFGeometry.TEXT_SIZE_KEY]
            fontwidth = text_info[RFGeometry.TEXT_FONTWIDTH_KEY]

            # Calculate the text y offset from bounding box
            text_y_offset_mm = size[0]/2 + RFGeometry.TEXT_OFFSET_MM
            if position_y == RFGeometry.TEXT_POSITION_OUTSIDE_TOP:
                text_y_mm = bb_min_mm.y - text_y_offset_mm # Because -y is up
            elif position_y == RFGeometry.TEXT_POSITION_OUTSIDE_BOT:
                text_y_mm = bb_max_mm.y + text_y_offset_mm
            elif position_y == RFGeometry.TEXT_POSITION_INSIDE:
                text_x_mm = (bb_max_mm.x - bb_min_mm.x) / 2
                text_y_mm = (bb_min_mm.y - bb_max_mm.y) / 2

            text_y_mm = RFGeometry.roundToBase(text_y_mm, 
                                        RFGeometry.TEXT_ROUND_PRECISION)
            text_x_mm = RFGeometry.roundToBase(text_x_mm, 
                                        RFGeometry.TEXT_ROUND_PRECISION)

            if layer == 'F.Fab':
                type=Text.TYPE_VALUE
            else:
                type = Text.TYPE_USER

            kicad_mod.append(Text(type=type, text=fp_name, 
                                    at=[text_x_mm, text_y_mm], layer=layer, 
                                    size=size, thickness=fontwidth))

    def _calcStubFabLines(self, stub_pad_width_mm, length_mm, angle_deg):
        # TODO: This should be moved to its own class and parameters should be
        # contained in self
        # Based on ADS definition: https://edadocs.software.keysight.com/pages/viewpage.action?pageId=5693198
        fab_line_width_mm = self.fab_line_width_mm
        theta_center_deg = 90 # Angle of center of radial stub
        alpha_deg = angle_deg/2

        D = stub_pad_width_mm/(2*math.sin(math.radians(alpha_deg)))
        Dx = D * cos(math.radians(alpha_deg)) # x component of D (radius to center of cut-off)
        r = D + length_mm # Total radius

        # Draw lines centered around x-axis then rotate later
        stub_fab_arc_start = Vector2D.from_polar(radius=r, angle=-alpha_deg, origin=(-Dx, 0))
        stub_fab_arc_end = Vector2D.from_polar(radius=r, angle=alpha_deg, origin=(-Dx, 0))
        fab_arc = Arc(start=stub_fab_arc_start, end=stub_fab_arc_end, 
                            center=(-D, 0), layer='F.Fab',
                            width=fab_line_width_mm)

        left_line = Line(start=(0, -stub_pad_width_mm/2), end=stub_fab_arc_start, 
                                layer='F.Fab', width=fab_line_width_mm)
        right_line = Line(start=(0, stub_pad_width_mm/2), end=stub_fab_arc_end, 
                                layer='F.Fab', width=fab_line_width_mm)
        center_line = Line(start=(0, stub_pad_width_mm/2), end=(0, -stub_pad_width_mm/2), 
                                layer='F.Fab', width=fab_line_width_mm)
        
        # Rotate to be centered at specified angle
        lines = [left_line, fab_arc, right_line, center_line]
        for line in lines:
            line.rotate(-theta_center_deg) # Angle is negative since -y is up

        return lines

    def calcRadialStubPolyNodes(self, length, trace_width, angle_deg, 
                                max_arc_seg_length_mm=0.01, origin_mm=(0, 0)):
        # TODO: This should be moved to its own class and parameters should be
        # contained in self
        # TODO: Probably a smarter way to define line segments is by defining 
        # a max error from an actual circle, measured as the distance between
        # the chord and the arc
        # Based on ADS definition: https://edadocs.software.keysight.com/pages/viewpage.action?pageId=5693198
        # calculate the length of the section that gets cut out by wi (i.e., D)
        alpha_rad = math.radians(angle_deg/2)
        D = trace_width/(2*math.sin(alpha_rad))
        r = D + length # Total radius

        origin_mm = Vector2D(origin_mm)
        Dx = D * cos(alpha_rad) # x component of D (radius to center of cut-off)
        theta_center_rad = math.pi/2 # Angle of center of radial stub
        transpose_by_mm = origin_mm - Vector2D.from_polar(Dx, theta_center_rad, 
                                                            use_degrees=False)

        # To get a straight line segment equal to c, calculate the straight
        # line segment over a theta change of dtheta = acos(1 - c^2/(2*r^2))
        dtheta_rad = math.acos(1 - max_arc_seg_length_mm**2/(2*r**2)) 
        # number of segments required to achieve dtheta
        num_segments = math.ceil(math.radians(angle_deg)/dtheta_rad)
        # Recalculate dtheta_d (which should now be slightly smaller)
        dtheta_rad = math.radians(angle_deg/num_segments)

        # Stub will be drawn from y = 0, face toward the top (centered around 
        # y-axis)
        arc_nodes = []
        theta_rad = alpha_rad # Set start angle to be +angle_deg/2
        for i in range(num_segments+1): # n + 1 for the starting point
            node = Vector2D.from_polar(r, theta_rad + theta_center_rad, 
                                        use_degrees=False)
            node += transpose_by_mm
            node.y *= -1 # Flip y since KiCAD defines -y as up direction
            arc_nodes.append(node)
            theta_rad -= dtheta_rad

        # Calculate start and end nodes for the Wi cutout section
        first_node = Vector2D.from_polar(D, theta_center_rad + alpha_rad, 
                                        use_degrees=False) + transpose_by_mm
        last_node = Vector2D.from_polar(D, theta_center_rad - alpha_rad, 
                                        use_degrees=False) + transpose_by_mm
        first_node.y *= -1
        last_node.y *= -1
        stub_nodes = [first_node] + arc_nodes + [last_node]

        return stub_nodes



    def generateRatRace(self, definition):
        # Load relevent config parameters
        fab_line_width_mm = self.fab_line_width_mm
        
        # Load in the rat race geometry parameters
        parameters = definition[RFGeometry.FP_PARAMS_KEY]
        units = parameters[RFGeometry.PARAMS_UNITS_KEY]
        radius = parameters[RFGeometry.PARAMS_RR_RADIUS_KEY]
        trace_width = parameters[RFGeometry.PARAMS_RR_WIDTH_KEY]
        radius_mm = self.unitToMm(radius, units)
        trace_width_mm = self.unitToMm(trace_width, units)

        # Determine soldermask parameters
        remove_soldermask = parameters.get(RFGeometry.PARAMS_NO_SMASK_KEY, True)
        if remove_soldermask:
            soldermask_offset = parameters.get(RFGeometry.PARAMS_SMASK_OFFSET_KEY, None)
            if soldermask_offset is not None:
                soldermask_offset_mm = self.unitToMm(soldermask_offset, units)
            else:
                soldermask_offset_mm = 3*trace_width_mm
        else:
            soldermask_offset_mm = 0
        
        # Get library information
        fp_name = definition[RFGeometry.FP_NAME_KEY]
        fp_name = fp_name.format(r=radius, w=trace_width, units=units)
        description = definition[RFGeometry.FP_DESCRIPTION_KEY]
        # As of V6.0.2, the only way to short pads together in the footprint is to
        # use graphic lines and make the first keyword 'net tie'
        keywords = "net tie " + definition[RFGeometry.FP_KEYWORDS_KEY]

        # Delete the unknown unit radius and width variables to make sure they 
        # don't get used
        del radius
        del trace_width

        kicad_mod = Footprint(fp_name)
        kicad_mod.setDescription(description)
        kicad_mod.setTags(keywords)
        kicad_mod.setAttribute('smd')

        # ---- Define copper layer ---- #
        # Define the pads for the ports
        port_pads = self.calcPortPads(radius_mm, trace_width_mm)
        for port_pad in port_pads:
            kicad_mod.append(Pad(**port_pad))

        # Add the rat race "trace"
        trace = Circle(center=[0, 0], radius=radius_mm, 
                        width=trace_width_mm, layer='F.Cu')
        kicad_mod.append(trace)

        # ---- Define fabrication layer ---- #
        # Add the fabrication layer
        fab_trace = Circle(center=[0, 0], radius=radius_mm, 
                            width=fab_line_width_mm, layer='F.Fab')
        kicad_mod.append(fab_trace)
        fab_port_polys = self.calcPortFabLines(port_pads)
        for fab_port_poly in fab_port_polys:
            kicad_mod.append(fab_port_poly)

        # ---- Define soldermask layer ---- #
        if remove_soldermask:
            mask = Circle(center=(0, 0), radius=radius_mm+soldermask_offset_mm,
                            layer='F.Mask', width=0, filled=True)
            kicad_mod.append(mask)

        # ---- Add Text ---- #
        self._appendTextRR(kicad_mod, radius_mm, soldermask_offset_mm, fp_name)

        return kicad_mod

    def _appendTextRR(self, kicad_mod, radius_mm, soldermask_offset_mm, fp_name):
        # References
        # TODO: Update so the F.Fab layer Ref** is dynamically sized
        # TODO: Once this is moved to its own class, all the parameters should
        #       be contained in self
        text_x = 0
        for text_info in self.ref_text_info:
            layer = text_info[RFGeometry.TEXT_LAYER_KEY]
            position_y = text_info[RFGeometry.TEXT_POSITION_KEY]
            size = text_info[RFGeometry.TEXT_SIZE_KEY]
            fontwidth = text_info[RFGeometry.TEXT_FONTWIDTH_KEY]

            # Calculate the text y offset from center
            text_y_offset = soldermask_offset_mm + \
                                size[0]/2 + RFGeometry.TEXT_OFFSET_MM
            text_y = RFGeometry.roundToBase(radius_mm + text_y_offset, 
                                        RFGeometry.TEXT_ROUND_PRECISION)

            if position_y == RFGeometry.TEXT_POSITION_OUTSIDE_BOT:
                text_y *= -1
            elif position_y == RFGeometry.TEXT_POSITION_INSIDE:
                text_y = 0

            if layer == 'F.SilkS':
                kicad_mod.append(Text(type=Text.TYPE_REFERENCE, text='REF**', 
                                      at=[text_x, -text_y], layer=layer, 
                                      size=size, thickness=fontwidth))
            else:
                kicad_mod.append(Text(type=Text.TYPE_USER, text='${REFERENCE}', 
                                      at=[text_x, -text_y], layer=layer, 
                                      size=size, thickness=fontwidth))

        # Values
        for text_info in self.value_text_info:
            layer = text_info[RFGeometry.TEXT_LAYER_KEY]
            position_y = text_info[RFGeometry.TEXT_POSITION_KEY]
            size = text_info[RFGeometry.TEXT_SIZE_KEY]
            fontwidth = text_info[RFGeometry.TEXT_FONTWIDTH_KEY]

            # Calculate the text y offset from center
            text_y_offset = soldermask_offset_mm + \
                                size[0]/2 + RFGeometry.TEXT_OFFSET_MM
            text_y = RFGeometry.roundToBase(radius_mm + text_y_offset, 
                                        RFGeometry.TEXT_ROUND_PRECISION)

            if position_y == RFGeometry.TEXT_POSITION_OUTSIDE_BOT:
                text_y *= -1
            elif position_y == RFGeometry.TEXT_POSITION_INSIDE:
                text_y = 0

            if layer == 'F.Fab':
                type=Text.TYPE_VALUE
            else:
                type = Text.TYPE_USER

            kicad_mod.append(Text(type=type, text=fp_name, 
                                    at=[text_x, -text_y], layer=layer, 
                                    size=size, thickness=fontwidth))

    def calcPortFabLines(self, port_pad_details):
        fab_width_mm = self.fab_line_width_mm
        trace_width = port_pad_details[0]['size']
        # Draw rectangle extruding from rat-race to indicate port:
        #  ======= <- Top
        #  ||    O <- Port
        #  ======= <- Bottom
        x_extension = trace_width*1.2
        y_offset_line = trace_width/2+fab_width_mm/2
        poly_nodes = [[0, y_offset_line], [0, -y_offset_line], 
                    [-x_extension, -y_offset_line], [-x_extension, y_offset_line],
                    [0, y_offset_line]]
        base_poly = Polygon(nodes=poly_nodes, layer='F.Fab', width=fab_width_mm)

        fab_ports = []
        # Define P1 to be on the left, P4 to be on the right
        for i in range(4):
            fab_port = base_poly.copy()

            # Get vector to this port pad and previous port pad (_m1)
            port_vector = Vector2D(port_pad_details[i]['at'])

            # --- Calculate port lines --- #
            fab_port.translate(port_vector)
            # Rotate
            angle_rad = -i*math.pi/3 # Angle of port
            fab_port.rotate(angle=angle_rad, origin=port_vector, use_degrees=False)
            fab_ports.append(fab_port)

        return fab_ports

    def calcPortPads(self, radius_mm, trace_width_mm):
        """Returns a list of dictionaries describing pad size and location. Has the
        form {'number': i,
            'type': Pad.TYPE_CONNECT,
            'layers': Pad.['F.Cu'],
            'shape': Pad.SHAPE_CIRCLE,
            'at': [pad_x, pad_y], 
            'size': pad_diameter,
            'type': Pad.TYPE_CONNECT}

        Args:
            radius_mm (float): Rat Race radius
            trace_width_mm (float): Rat race line width

        Returns:
            list: list of dicts describing port pads
        """
        port_pads = []
        pad_diameter = trace_width_mm

        # Define P1 to be on the left, P4 to be on the right
        for i in range(4):
            angle_rad = i*math.pi/3
            pad_x = -radius_mm * math.cos(angle_rad)
            pad_y = radius_mm * math.sin(angle_rad)
            port_pads.append({'number': i+1,
                            'type': Pad.TYPE_SMT,
                            'layers': ['F.Cu'],
                            'shape': Pad.SHAPE_CIRCLE,
                            'at': [pad_x, pad_y], 
                            'size': pad_diameter,}
                            )

        return port_pads

    @staticmethod
    def roundToBase(value, base):
        return round(value / base) * base

    @staticmethod
    def mmToMils(mm):
        return mm * 1.0/0.0254

    @staticmethod
    def milsToMm(mils):
        return mils * 0.0254

    @staticmethod
    def unitToMm(value, unit):
        """Convert the given value in the given units to mm

        Args:
            value (int, float): value to convert
            unit (string): unit of given value

        Raises:
            ValueError: raised if unit is not supported

        Returns:
            float: given value converted to units of mm
        """
        if unit not in RFGeometry._VALID_UNITS:
            err_str = "Unit {:s} not support, use one of: {:s}".format(
                unit, RFGeometry._VALID_UNITS)
            raise ValueError(err_str)
        
        if unit == 'mm':
            return value
        elif unit == 'mils':
            return RFGeometry.milsToMm(value)

if __name__ == "__main__":
    # radius_mm = 4
    # width_mm = 0.3

    # generateRatRace(radius_mm, width_mm)
    parser = argparse.ArgumentParser(description='Give a footprint description file and a config (.yaml) to generate RF geometry.')
    parser.add_argument('definition_file', metavar='file', type=str, nargs='?',
                        help='File holding the geometry descriptions')
    parser.add_argument('--global_config', type=str, nargs='?', 
                        help='The config file defining basic layer parameters. (KLC)',
                        default='../tools/global_config_files/config_KLCv3.0.yaml')
    parser.add_argument('--lib_root_path', type=str, nargs='?', default='./')
    args = parser.parse_args()

    generator = RFGeometry(args.definition_file, args.global_config, args.lib_root_path)
    generator.makeFootprints()